package com.mygdx.solarsystem.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.mygdx.solarsystem.SolarSytsem;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(800, 640);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new SolarSytsem();
        }
}