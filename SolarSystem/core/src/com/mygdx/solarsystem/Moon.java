package com.mygdx.solarsystem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by joseph on 23.10.2017.
 */

public class Moon {

    Texture moonImage;
    int moonImageSize;

    Planet parentPlanet;
    int orbitRadius;

    int x;
    int y;
    int period;

    float phi = 0;

    Rectangle pole = new Rectangle();

    public Moon(Texture moonImage, int moonImageSize, Planet parentPlanet, int orbitRadius, int period) {
        this.moonImage = moonImage;
        this.moonImageSize = moonImageSize;
        this.parentPlanet = parentPlanet;
        this.orbitRadius = orbitRadius;
        this.period = period;

        x = parentPlanet.getX() + (int) (orbitRadius * MathUtils.cos(0));
        y = parentPlanet.getY() + (int) (orbitRadius * MathUtils.sin(0));

        pole.x = x - moonImageSize /2;
        pole.y = y - moonImageSize /2;

    }

    public void move() {
        if (period !=0) {
            phi += MathUtils.PI2 / period * Gdx.graphics.getDeltaTime();
        }

        x = parentPlanet.getX() + (int) (orbitRadius * MathUtils.cos(phi));
        y = parentPlanet.getY() + (int) (orbitRadius * MathUtils.sin(phi));

        pole.x = x - moonImageSize / 2;
        pole.y = y - moonImageSize / 2;
    }
}
