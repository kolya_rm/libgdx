package com.mygdx.solarsystem;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class SolarSytsem extends ApplicationAdapter {

	OrthographicCamera camera;
	SpriteBatch batch;

	Stage stage;
	Texture buttonTexture;
	TextureRegion buttonTextureRegion;
	TextureRegionDrawable buttonTextureRegionDrawable;
	ImageButton imageButton;

	boolean gamePaused;
	Star sun;
	Planet earth;
	Moon moon;
	Planet mars;


	@Override
	public void create () {
		buttonTexture = new Texture("button_pause.png");
		buttonTextureRegion = new TextureRegion(buttonTexture);
		buttonTextureRegionDrawable = new TextureRegionDrawable(buttonTextureRegion);

		imageButton = new ImageButton(buttonTextureRegionDrawable);
		imageButton.setPosition(716,20);

		stage = new Stage(new ScreenViewport());
		stage.addActor(imageButton);
		Gdx.input.setInputProcessor(stage);

		imageButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y){
				gamePaused = !gamePaused;
			}
		});

		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		batch = new SpriteBatch();

		gamePaused = true;
		sun = new Star(new Texture("sun.png"), 96, 400, 240);
		earth = new Planet(new Texture("earth.png"), 48, sun, 100, 10);
		moon = new Moon(new Texture("moon.png"),16, earth, 35, 3);
		mars = new Planet(new Texture("mars.png"),32, sun, 200, 25);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();

		camera.update();

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(sun.starImage, sun.pole.x, sun.pole.y);
		batch.draw(earth.planetImage, earth.pole.x, earth.pole.y);
		batch.draw(moon.moonImage, moon.pole.x, moon.pole.y);
		batch.draw(mars.planetImage, mars.pole.x, mars.pole.y);
		batch.end();

		if (!gamePaused) {
			earth.move();
			moon.move();
			mars.move();
		}
	}

	@Override
	public void dispose () {
		super.dispose();
		batch.dispose();
		stage.dispose();
	}
}