package com.mygdx.solarsystem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by joseph on 23.10.2017.
 */

public class Planet {

    Texture planetImage;
    int planetImageSize;

    Star parentStar;
    int orbitRadius;

    int x;
    int y;
    int period;
    float phi = 0;


    Rectangle pole = new Rectangle();

    public Planet(Texture planetImage, int planetImageSize, Star parentStar, int orbitRadius, int period) {
        this.planetImage = planetImage;
        this.planetImageSize = planetImageSize;
        this.parentStar = parentStar;
        this.orbitRadius = orbitRadius;
        this.period = period;

        x = parentStar.getX() + (int) (orbitRadius * MathUtils.cos(phi));
        y = parentStar.getY() + (int) (orbitRadius * MathUtils.sin(phi));

        pole.x = x - planetImageSize / 2;
        pole.y = y - planetImageSize / 2;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void move() {
        if (period !=0) {
            phi += MathUtils.PI2 / period * Gdx.graphics.getDeltaTime();
        }

        x = parentStar.getX() + (int) (orbitRadius * MathUtils.cos(phi));
        y = parentStar.getY() + (int) (orbitRadius * MathUtils.sin(phi));

        pole.x = x - planetImageSize / 2;
        pole.y = y - planetImageSize / 2;
    }
}
