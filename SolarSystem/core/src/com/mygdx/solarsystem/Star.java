package com.mygdx.solarsystem;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by joseph on 23.10.2017.
 */

public class Star {

    Texture starImage;
    int starImageSize;

    int x;
    int y;

    Rectangle pole = new Rectangle();

    public Star(Texture starImage, int starImageSize, int x, int y) {
        this.starImage = starImage;
        this.starImageSize = starImageSize;
        this.x = x;
        this.y = y;

        pole.x = x - this.starImageSize / 2;
        pole.y = y - this.starImageSize / 2;;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
